#include "poisson_disc_sampler.h"

#include <godot_cpp/variant/utility_functions.hpp>

void PoissonDiscSampler::_bind_methods() {
  // Configuration
  ClassDB::bind_method(D_METHOD("get_extents"),
                       &PoissonDiscSampler::get_extents);
  ClassDB::bind_method(D_METHOD("set_extents", "extents"),
                       &PoissonDiscSampler::set_extents);
  ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "extents"), "set_extents",
               "get_extents");

  ClassDB::bind_method(D_METHOD("get_minimum_distance"),
                       &PoissonDiscSampler::get_minimum_distance);
  ClassDB::bind_method(D_METHOD("set_minimum_distance", "minimum_distance"),
                       &PoissonDiscSampler::set_minimum_distance);
  ADD_PROPERTY(PropertyInfo(Variant::FLOAT, "minimum_distance"),
               "set_minimum_distance", "get_minimum_distance");

  ClassDB::bind_method(D_METHOD("get_limit"), &PoissonDiscSampler::get_limit);
  ClassDB::bind_method(D_METHOD("set_limit", "limit"),
                       &PoissonDiscSampler::set_limit);
  ADD_PROPERTY(PropertyInfo(Variant::INT, "limit"), "set_limit", "get_limit");

  // Methods.
  ClassDB::bind_method(D_METHOD("finish"), &PoissonDiscSampler::finish);

  ClassDB::bind_method(D_METHOD("next"), &PoissonDiscSampler::next);

  ClassDB::bind_method(D_METHOD("get_points"), &PoissonDiscSampler::get_points);
}

PoissonDiscSampler::PoissonDiscSampler()
    : _sampler{_extents, _minimum_distance, _limit} {}

PoissonDiscSampler::~PoissonDiscSampler() {}

//
// Methods
//
Variant PoissonDiscSampler::next() {
  _check_config();
  auto const next = _sampler.next();
  if (next.has_value()) {
    return Vector2{next.value().x, next.value().y};
  }

  return nullptr;
}

void PoissonDiscSampler::finish() {
  _check_config();
  _sampler.finish();
}

PackedVector2Array PoissonDiscSampler::get_points() const {
  PackedVector2Array gd_points{};

  for (auto &&point : _sampler.points()) {
    gd_points.push_back(Vector2(point.x, point.y));
  }

  return gd_points;
}

void PoissonDiscSampler::_check_config() {
  if (_config_changed) {
    _sampler = pds::sampler<float>{_extents, _minimum_distance,
                                   static_cast<std::size_t>(_limit)};
    _config_changed = false;
  }
}

//
// Properties
//
void PoissonDiscSampler::set_extents(float const extents) {
  if (Math::is_equal_approx(_extents, extents)) {
    return;
  }

  _config_changed = true;
  _extents = extents;
}

float PoissonDiscSampler::get_extents() const { return _extents; }

void PoissonDiscSampler::set_minimum_distance(float const minimum_distance) {
  if (Math::is_equal_approx(_minimum_distance, minimum_distance)) {
    return;
  }

  _config_changed = true;
  _minimum_distance = minimum_distance;
}

float PoissonDiscSampler::get_minimum_distance() const {
  return _minimum_distance;
}

void PoissonDiscSampler::set_limit(int64_t const limit) {
  if (_limit == limit) {
    return;
  }

  _config_changed = true;
  _limit = limit;
}

int64_t PoissonDiscSampler::get_limit() const { return _limit; }
