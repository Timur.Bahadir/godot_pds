#ifndef PDS_REGISTER_TYPES_H
#define PDS_REGISTER_TYPES_H

#include <godot_cpp/core/class_db.hpp>

using namespace godot;

void initialize_pds_module(ModuleInitializationLevel p_level);
void uninitialize_pds_module(ModuleInitializationLevel p_level);

#endif // PDS_REGISTER_TYPES_H