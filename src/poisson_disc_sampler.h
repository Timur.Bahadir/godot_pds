#pragma once

#include <godot_cpp/classes/global_constants.hpp>
#include <godot_cpp/classes/node.hpp>

#include <godot_cpp/core/binder_common.hpp>

#include <pds/pds.hpp>

using namespace godot;

class PoissonDiscSampler : public Node {
  GDCLASS(PoissonDiscSampler, Node);

protected:
  static void _bind_methods();

private:
  void _check_config();

  pds::sampler<float> _sampler;

  bool _config_changed{false};
  float _extents{100.0f};
  float _minimum_distance{10.0f};
  int64_t _limit{30};

public:
  PoissonDiscSampler();
  ~PoissonDiscSampler();

  // Configuration
  void set_extents(float const extents);
  float get_extents() const;

  void set_minimum_distance(float const minimum_distance);
  float get_minimum_distance() const;

  void set_limit(int64_t const limit);
  int64_t get_limit() const;

  // Methods
  void finish();
  Variant next();

  // Data
  PackedVector2Array get_points() const;
};
