# Poisson-Disc Sampling for Godot

## Build

### Requirements

You should be able to compile Godot itself (the requirements overlap and Godot has them documented far better).

Just follow this guide [here](https://docs.godotengine.org/en/stable/development/compiling/compiling_for_windows.html).

### Prepare

Checkout this repository

```bash
git checkout <TODO: insert link>
```

Initialize the submodules

```bash
git submodule update --init --recursive
```

Make sure that the _libs/_ folder has two sub-folders, _godot-cpp_ and _pds_.
Then make sure that the _godot-cpp_ folder has a subfolder _godot-headers_ _with content_.

### Create the bindings

NOTE: Might not be required anymore

Next we have to build the bindings, this is done via the command below.
Run it in the _libs/godot-cpp/_ directory.

```bash
scons platform=<platform> generate_bindings=yes -j<num_of_cores>
```

### Build the plugin binaries

Now you should be able to build the plugin by running the following in the
root directory:

```bash
scons platform=<platform> -j<num_of_cores>
```

### Package everything into a godot plugin

The final structure of the plugin should follow this layout:

```bash
|/
|->addons
  |->godot-pds
    |->bin/
      |->linux64/
        |->libgd.linux.debug.64.so
      |->win64/
        |->libgd.windows.debug.64.dll
      |->.../
    |->config/
      |->godot_pds.gdnlib
      |->PoissonDiscSampler.gdns
    |->scenes/
      |->PoissonDiscSampler.tscn
    |->LICENSE.md
    |->plugin.cfg
    |->plugin.gd
    |->README.md
```
