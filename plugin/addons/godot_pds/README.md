# Poisson-Disc Sampling for Godot

## Usage

```python
sampler.extents = max(get_viewport_rect().size.x, get_viewport_rect().size.y)
sampler.minimum_distance = 20.0
sampler.limit = 30
sampler.finish()

for point in sampler.get_points():
  ... do something with point ...
```
