extends Node2D

@onready
var sampler: PoissonDiscSampler = PoissonDiscSampler.new()

var initial_points: Array[Vector2] = []

var points: Array[Vector2] = []

func _ready():
	sampler.extents = max(get_viewport_rect().size.x, get_viewport_rect().size.y)
	sampler.minimum_distance = 20.0
	sampler.limit = 30
	sampler.finish()

	for point in sampler.get_points():
		initial_points.push_back(point)

	sampler.minimum_distance = 20.0

	for point in sampler.get_points():
		points.push_back(point)

	queue_redraw()


func _on_point_timer_timeout():
	var next = sampler.next()
	points.push_back(next)
	queue_redraw()


func _draw():
	for point in initial_points:
		draw_circle(point, 9.0, Color.WHITE)

	for point in points:
		draw_circle(point, 9.0, Color.RED)
