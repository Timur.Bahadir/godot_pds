$build_root = "bin/"
$target_root = "plugin/bin/"

New-Item -Path . -Name $target_root -ItemType Directory -Force

Get-ChildItem -Path ${build_root} .\* -include ('*.dll', '*.framework', '*.so') -recurse |
ForEach-Object {
  $name = $_.Name
  Write-Output $name
  Copy-Item -Path $_.FullName -Destination "$target_root/$name"
}
