scons target=template_debug platform=windows
scons target=template_release platform=windows

scons target=template_debug platform=linux
scons target=template_release platform=linux

scons target=template_debug platform=macos
scons target=template_release platform=macos

scons target=template_debug platform=javascript
scons target=template_release platform=javascript

scons target=template_debug platform=android
scons target=template_release platform=android

scons target=template_debug platform=ios
scons target=template_release platform=ios
